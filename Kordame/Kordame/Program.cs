﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kordame
{
    class Program
    {
        static void Main(string[] args)
        {
            double arv = 77;
            arv += Math.Pow(7,3);
            Tryki(arv * 4 + Liida(4,7));
            Program.Tryki("Arvutatud");

            // funktsioone ja meetodeid on kahte tõugu
            // objekti omad ja klassi omad

            string s = arv.ToString();

            int b = int.Parse(s);

            string nimi = "Henn Sarv";
            string proov = nimi
                //.ToUpper()
                .Replace("Henn", "Hennuke")
                .ToUpper()
                .Split(' ')
                [0]
                ;
            
            Tryki(proov);

            // teeme nime suurteks tähtedeks
            string valeNimi = "anton HANSEN TammSaare";
            string[] osad = valeNimi.ToLower().Split(' ');
            for (int i = 0; i < osad.Length; i++)
            {
                osad[i] = osad[i].Substring(0, 1).ToUpper() + osad[i].Substring(1);
            }
            string õigeNimi = string.Join(" ", osad);
            Tryki(õigeNimi);

            List<string> uuedOsad = new List<string>();
            foreach (var x in osad)
                uuedOsad.Add(x.Substring(0, 1).ToUpper() + x.Substring(1));
            õigeNimi = String.Join(" ", uuedOsad);

            õigeNimi = "";
            foreach (var x in osad)
                õigeNimi += x.Substring(0, 1).ToUpper() + x.Substring(1) + " ";
            õigeNimi = õigeNimi.Trim();

            StringBuilder sb = new StringBuilder();

            foreach (var x in osad)
                sb.Append( x.Substring(0, 1).ToUpper() + x.Substring(1) + " ");
            õigeNimi = sb.ToString().Trim();

        }

        static int Liida(int x, int y)
        {
            return x + y + 100;
        }

        static void Tryki(object x)
        {
            Console.WriteLine(x);
        }
    }
}
