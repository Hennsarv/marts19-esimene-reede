﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KordameKogumeid
{
    enum Mast { Risti, Ruutu, Ärtu, Poti}
    enum Tugevus { Kaks, Kolm, Neli, Viis, Kuus, Seitse, Kaheksa, Üheksa, Kümme, Soldat, Emand, Kuningas, Äss}

    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();

            List<int> kaardid = Enumerable.Range(0, 52).ToList();

            Console.WriteLine("\npakk enne segamist\n");
            for (int i = 0; i < kaardid.Count; i++)
                Console.Write($"\t{kaardid[i]} {(i % 4 == 3 ? "\n" : "")}"); // mis kuradi asi see on

            // siin vahepeal tuleks pakk ära segada
            // kas keegi on välja mõelnud, kuidas segada

            List<int> segatudpakk = new List<int>();
            while(kaardid.Count > 0)
            {
                int i = r.Next(kaardid.Count);
                segatudpakk.Add(kaardid[i]);
                kaardid.RemoveAt(i);
            }

            kaardid = segatudpakk;

            Console.WriteLine("\npakk peale segamist\n");
            for (int i = 0; i < kaardid.Count; i++)
                Console.Write($"\t{kaardid[i]} {(i % 4 == 3 ? "\n" : "")}");

            

            Console.WriteLine("\nnumbrite asemele kaardid\n");
            for (int i = 0; i < kaardid.Count; i++)
                Console.Write($"\t{(Mast)(kaardid[i]/13)} {(Tugevus)(kaardid[i]%13)} {(i % 4 == 3 ? "\n" : "")}");



        }


    }
}
