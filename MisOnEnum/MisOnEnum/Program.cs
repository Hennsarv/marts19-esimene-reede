﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnEnum
{
    enum Mast { Risti, Ruutu, Ärtu, Poti}

    [Flags]
    enum Tunnus { Suur = 1, Punane = 2, Puust = 4, Kolisev = 8}

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine((Tunnus)7);

            Tunnus t = Tunnus.Puust | Tunnus.Punane;
            t |= Tunnus.Kolisev;
            t ^= Tunnus.Puust;
            Console.WriteLine(t);

            if ((t & Tunnus.Punane) == Tunnus.Punane) Console.WriteLine("on punane");
            else Console.WriteLine("ei ole punane");

            
        }
    }
}
